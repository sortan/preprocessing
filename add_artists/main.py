import requests

BASE_URL = 'https://api.sortan-core.com/'
artists = [
  'Sophia kao	',
  'VannDa	',
  'Suly Pheng	',
  'ខេមរៈ សិរីមន្ត	',
  'ឱក សុគន្ធកញ្ញា	',
  'ព្រាប សុវត្ថិ	',
  'Tena	',
  'P-Sand X Brothers Official	',
  '4T5',
  'Bross La	',
  'GLOMYY VINCENT	',
  'RXTHY	',
  'Tempo Tris	',
  'Van Chesda	',
  'DJ Chee	',
  'Euan Gray	',
  'Nevermind Band	',
  'Chen ចេន	',
  'MUSTACHE BAND	',
  'ចាន់ សុបញ្ញា	',
  'Manith Jupiter	',
  'VI70',
]

for artist in artists:
  r = requests.post(f'{BASE_URL}/artists', json={"name": artist})
  print(r.json())
