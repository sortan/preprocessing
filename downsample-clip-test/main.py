import os
from pydub import AudioSegment

DOWNLOAD_PATH = os.getcwd() + '/download'
SONG_NAME = 'Suly Pheng - មិនច្បាស់ជាមួយអូន ft. Olica (Lyrics Video)_Suly Pheng.wav'
SAMPLE_RATE = 11025

t1 = 2 * 1000 #Works in milliseconds
t2 = 8 * 1000
newAudio = AudioSegment.from_wav(os.path.join(DOWNLOAD_PATH, SONG_NAME))
newAudio = newAudio[t1:t2]
newAudio.export('stereo.wav', format="wav")

newAudio = newAudio.set_channels(1)
newAudio = newAudio.low_pass_filter(SAMPLE_RATE / 2)
newAudio = newAudio.set_frame_rate(SAMPLE_RATE)
newAudio.export('mono.wav', format="wav")
