import numpy as np
from pydub import AudioSegment
def load_wav(file_path: str):
	# load as AudioSegment
	return AudioSegment.from_wav(file_path)

def stereo_to_mono(audio: AudioSegment):
	if audio.channels == 1:
		return audio
	elif audio.channels == 2:
		return audio.set_channels(1)
	else:
		raise ValueError('audio channel can only be 1 or 2!')

def is_audio_mono(audio: AudioSegment):
  return audio.channels == 1

def is_sample_rate_valid(sample_rate):
  return sample_rate == VALID_SAMPLE_RATE

# Might not use.
def low_pass_filter(audio: AudioSegment, cut_off: int) -> AudioSegment:
	return audio.low_pass_filter(cut_off)

def downsample(audio: AudioSegment, frame_rate: int):
	return audio.set_frame_rate(frame_rate)

def get_wav_info(audio: AudioSegment):
	# convert the pydub samples array to numpy array for later uses.
	return np.array(audio.get_array_of_samples()), audio.frame_rate

def get_max_amplitude_between_freq(time_frame: np.ndarray, min_freq: int, max_freq: int):
  amplitudes = time_frame[min_freq : max_freq]
  if amplitudes.size > 0:
    return amplitudes.max()
  else:
    return 0

class HighestAmplitudePeak:
  def __init__(self, time, frequency):
    self.time = time
    self.frequency = frequency
  
  def __repr__(self):
    return f'time: {self.time}, frequency: {self.frequency}'

class FingerPrint:
  def __init__(
  self,
  zone_point: HighestAmplitudePeak,
  anchor_point: HighestAmplitudePeak,
  song_id: int
  ):
    '''
    We would just store as `HighestAmplitudePeak` but when we transfer to the server code,
    we don't want any confusion. So it is more clear if we can map this code the SQL table
    TODO: target-zone is just a concept, I (sovichea) don't thing it is nessary to have
    a table that decticate to it. the only problem is for the first 5 points having the
    same point, and the last 5 points having the same point as anchor point. If we know
    the anchor point's frequency and time, we will know the target zone since most will
    be tied to 1 zone only. And also we don't need to store all the points in an array
    to and marks as target zone either.
    '''
    self.anchor_frequency = anchor_point.frequency
    self.anchor_time = anchor_point.time
    self.point_frequency = zone_point.frequency
    self.point_time = zone_point.time
    self.time_difference = self.point_time - self.anchor_time
    self.song_id = song_id
  
  def __repr__(self):
    return f'anchor: [time: {self.anchor_time}, freq: {self.anchor_frequency}], point: [time: {self.point_time}, freq: {self.point_frequency}]'
