from pydub import AudioSegment
from pydub.generators import WhiteNoise
import os
import random

SAMPLE_RATE = 44100
DOWNLOAD_PATH = os.getcwd() + '/download'
FORMAT = 'wav'
BASE_URL = 'http://127.0.0.1:5000/'
AUDIO_SAMPLES = [
  'កុំមានអ្នកទី៣ [Official Lyric Video]_P-Sand X Brothers Official.wav'
]

def set_to_target_level(sound, target_level):
  difference = target_level - sound.dBFS
  return sound.apply_gain(difference)

# sec = random.randint(0, 200)
sec = 39.5

t1 = sec * 1000 #Works in milliseconds
t2 = (sec + 10) * 1000
newAudio = AudioSegment.from_wav(os.path.join(DOWNLOAD_PATH, AUDIO_SAMPLES[0]))
newAudio = newAudio.low_pass_filter(SAMPLE_RATE/2)
newAudio = newAudio.set_frame_rate(SAMPLE_RATE)
newAudio = newAudio[t1:t2]

# noise = WhiteNoise().to_audio_segment(duration=len(newAudio))
# noise = set_to_target_level(noise, -20)

# combined = newAudio.overlay(noise)

# newAudio = newAudio.set_channels(1)
# newAudio = newAudio.low_pass_filter(44100/2)
# newAudio = newAudio.set_frame_rate(44100)

newAudio.export(f'segment-clip/{AUDIO_SAMPLES[0]}_clip.wav', format="wav") #Exports to a wav file in the current path.
# combined.export(f'segment-clip/{AUDIO_SAMPLES[0]}_noisy.wav', format="wav") #Exports to a wav file in the current path.
