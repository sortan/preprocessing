import os
import requests
import csv

BASE_URL = 'http://127.0.0.1:5000'
MAIN_DIR = 'segment-clip/10-44100'
SUB_DIRS = [
  'hot-boy',
  'nek-ti-3',
  'khok-khan',
  'psycho',
  'ram-vong-angkor'
]

with open('test-logs.csv', 'w', newline='') as csvfile:
  fieldnames = ['actual_name', 'clip', 'time_to_compute', 'result_name', 'request_success']
  writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
  writer.writeheader()

  for directory in SUB_DIRS:
    for clip in os.listdir(os.path.join(MAIN_DIR, directory)):
      if clip != '.DS_Store':
        with open(os.path.join(MAIN_DIR, directory, clip), 'rb') as f:
          r = requests.post(f'{BASE_URL}/musics/search', files={'file': f})
          response = r.json()
          if response['success']:
            writer.writerow({
              'actual_name': directory,
              'clip': clip,
              'time_to_compute': response['message']['time'],
              'result_name': response['message']['name'].strip(),
              'request_success': True
            })
          else:
            writer.writerow({
              'actual_name': directory,
              'clip': clip,
              'time_to_compute': '',
              'result_name': '',
              'request_success': False
            })

