
from pydub import AudioSegment
import numpy as np
import pylab
import statistics
from matplotlib import pyplot as plt
from helpers import (
  HighestAmplitudePeak,
  get_max_amplitude_between_freq,
  FingerPrint,
  low_pass_filter,
  stereo_to_mono,
  downsample
)


audio = AudioSegment.from_wav('segment-clip/កុំមានអ្នកទី៣ [Official Lyric Video]_P-Sand X Brothers Official.wav_clip.wav')
audio = stereo_to_mono(audio)
frames, frame_rate = np.array(audio.get_array_of_samples()), audio.frame_rate

fig, (ax1, ax2) = plt.subplots(nrows=2)

spectrum, frequencies, times, img = ax1.specgram(
  frames,
  Fs=frame_rate,
  # as defined in the paper we going to use 1024 bin.
  window= np.hamming(4096),
  NFFT= 4096,
  # in the recent article, it mention to use the spectrum magnitude.
  # mode='magnitude',
  # need to add overlap 0 too to get what the paper wanted.
  noverlap = 0,
)

highest_peaks = []

for index in range(times.size):
  time_frame = spectrum[:, index]
  time = times[index]

  # get the the 6 bands of time frame.
  vl_band = get_max_amplitude_between_freq(time_frame, 0, 40)
  l_band = get_max_amplitude_between_freq(time_frame, 40, 80)
  lm_band = get_max_amplitude_between_freq(time_frame, 80, 160)
  m_band = get_max_amplitude_between_freq(time_frame, 160, 320)
  mh_band = get_max_amplitude_between_freq(time_frame, 320, 640)
  h_band = get_max_amplitude_between_freq(time_frame, 640, 2049)
  # calculate the amplitude mean of the time frame.
  bands = np.array([
    vl_band,
    l_band,
    lm_band,
    m_band,
    mh_band,
    h_band
  ])
  if bands.max() > 0:
    mean_amplitude = statistics.mean(bands)
    '''
    find the index of band that the amplitude is higher then the mean.
    in the paper, the mean is multiply by a coefficient.
    it is also mention that this particular algorithm is not very efficient.
    it can be change to `logarithmic sliding window and to keep only the most powerful
    frequencies above the mean + the standard deviation (multiplied by a coefficient) of
    a moving part of the song.`
    '''
    qualify_band_idexes = np.where(bands > mean_amplitude)[0]
    # get the qualify amplitude in the time frame.
    qualify_amplitudes = bands[qualify_band_idexes]
    # get the frequency indexes in the time frame that will product the qualify amplitude.
    # high_peaks_freq_idexes = np.sort(np.nonzero(np.in1d(time_frame, qualify_amplitudes))[0])
    high_peaks_freq_idexes = np.sort(np.nonzero(np.in1d(time_frame, bands))[0])
    '''
    Before the grouping into target zone, we need to sort the our time-frequency pair first.
    - For each time-frame we it is sorted by low freq to high freq.
    - For each spectrogram, we sorted it by lowest-time to highest-time.
    '''
    # get frequenies by index in the time frame and sort it.
    high_peaks_freq = np.sort(frequencies[high_peaks_freq_idexes])
    # loop through those index and create a coordinate for it (time, frequency)
    for freq in high_peaks_freq:
      highest_peaks.append(HighestAmplitudePeak(
        time,
        freq
      ))
# form the x axis for ploting in our case x axis is time.
x_axis1 = [peak.time for peak in highest_peaks]
# form the y axis for ploting in our case y axis is frequency.
y_axis1 = [peak.frequency for peak in highest_peaks]

audio = AudioSegment.from_wav('segment-clip/compare_clip.wav')
audio = stereo_to_mono(audio)
frames, frame_rate = np.array(audio.get_array_of_samples()), audio.frame_rate

spectrum, frequencies, times, img1 = ax2.specgram(
  frames,
  Fs=frame_rate,
  # as defined in the paper we going to use 1024 bin.
  window= np.hamming(4096),
  NFFT= 4096,
  # in the recent article, it mention to use the spectrum magnitude.
  # mode='magnitude',
  # need to add overlap 0 too to get what the paper wanted.
  noverlap = 0
)

highest_peaks = []

for index in range(times.size):
  time_frame = spectrum[:, index]
  time = times[index]

  # get the the 6 bands of time frame.
  vl_band = get_max_amplitude_between_freq(time_frame, 0, 40)
  l_band = get_max_amplitude_between_freq(time_frame, 40, 80)
  lm_band = get_max_amplitude_between_freq(time_frame, 80, 160)
  m_band = get_max_amplitude_between_freq(time_frame, 160, 320)
  mh_band = get_max_amplitude_between_freq(time_frame, 320, 640)
  h_band = get_max_amplitude_between_freq(time_frame, 640, 2049)
  # calculate the amplitude mean of the time frame.
  bands = np.array([
    vl_band,
    l_band,
    lm_band,
    m_band,
    mh_band,
    h_band
  ])
  if bands.max() > 0:
    mean_amplitude = statistics.mean(bands)
    '''
    find the index of band that the amplitude is higher then the mean.
    in the paper, the mean is multiply by a coefficient.
    it is also mention that this particular algorithm is not very efficient.
    it can be change to `logarithmic sliding window and to keep only the most powerful
    frequencies above the mean + the standard deviation (multiplied by a coefficient) of
    a moving part of the song.`
    '''
    qualify_band_idexes = np.where(bands > mean_amplitude)[0]
    # get the qualify amplitude in the time frame.
    qualify_amplitudes = bands[qualify_band_idexes]
    # get the frequency indexes in the time frame that will product the qualify amplitude.
    # high_peaks_freq_idexes = np.sort(np.nonzero(np.in1d(time_frame, qualify_amplitudes))[0])
    high_peaks_freq_idexes = np.sort(np.nonzero(np.in1d(time_frame, bands))[0])
    '''
    Before the grouping into target zone, we need to sort the our time-frequency pair first.
    - For each time-frame we it is sorted by low freq to high freq.
    - For each spectrogram, we sorted it by lowest-time to highest-time.
    '''
    # get frequenies by index in the time frame and sort it.
    high_peaks_freq = np.sort(frequencies[high_peaks_freq_idexes])
    # loop through those index and create a coordinate for it (time, frequency)
    for freq in high_peaks_freq:
      highest_peaks.append(HighestAmplitudePeak(
        time,
        freq
      ))
# form the x axis for ploting in our case x axis is time.
x_axis2 = [peak.time for peak in highest_peaks]
# form the y axis for ploting in our case y axis is frequency.
y_axis2 = [peak.frequency for peak in highest_peaks]

'''Plotting the filtered spectrogram.'''
# plt.clf()
ax1.cla()
ax2.cla()
ax1.title.set_text('segment from origin')
ax1.scatter(x_axis1, y_axis1, s=1, c='red')
ax2.title.set_text('recorded')
ax2.scatter(x_axis2, y_axis2, s=1, c='black')


plt.show()
