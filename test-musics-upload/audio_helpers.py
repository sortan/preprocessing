from pydub import AudioSegment
import numpy as np

VALID_SAMPLE_RATE = 11025
VALID_MAX_FREQUENCY = VALID_SAMPLE_RATE / 2
MONO = 1

def load_wav(file_path: str):
	# load as AudioSegment
	return AudioSegment.from_wav(file_path)

def stereo_to_mono(audio: AudioSegment):
	if audio.channels == 1:
		return audio
	elif audio.channels == 2:
		return audio.set_channels(1)
	else:
		raise ValueError('audio channel can only be 1 or 2!')

def is_audio_mono(audio: AudioSegment):
  return audio.channels == 1

def is_sample_rate_valid(sample_rate):
  return sample_rate == VALID_SAMPLE_RATE

# Might not use.
def low_pass_filter(audio: AudioSegment, cut_off: int) -> AudioSegment:
	return audio.low_pass_filter(cut_off)

def downsample(audio: AudioSegment, frame_rate: int):
	return audio.set_frame_rate(frame_rate)

def get_wav_info(audio: AudioSegment):
	# convert the pydub samples array to numpy array for later uses.
	return np.array(audio.get_array_of_samples()), audio.frame_rate
