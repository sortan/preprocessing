from audio_helpers import (
  load_wav,
  stereo_to_mono,
  low_pass_filter,
  downsample,
  get_wav_info
)
import requests
import os
import io
from pydub.utils import mediainfo
import json

VALID_SAMPLE_RATE = 44100
VALID_MAX_FREQUENCY = VALID_SAMPLE_RATE / 2
MONO = 1

DOWNLOAD_PATH = os.getcwd() + '/download'
FORMAT = 'wav'
BASE_URL = 'https://api.sortan-core.com/'
# AUDIO_SAMPLES = [
#   'Bigdoqqq - ធ្វើខុស ( Tve Khos ) Noly Record Cover )_Tena Khimphun.wav',
#   'មកក្រោយគេ (COME LATE) BY MUSTACHE BAND_Mustache Official Channel.wav'
# ]

file_dir = os.listdir(DOWNLOAD_PATH)

for i in range(25):
  music_path = file_dir[i]
  audio = load_wav(os.path.join(DOWNLOAD_PATH, music_path))
  audio = stereo_to_mono(audio)
  audio = low_pass_filter(audio, VALID_MAX_FREQUENCY)
  audio = downsample(audio, VALID_SAMPLE_RATE)
  audio.export(music_path, format=FORMAT)
  meatadata = mediainfo(music_path)
  # with open('metadata.json', 'w') as f:
  #   json.dump(meatadata, f)


  with open(music_path, 'rb') as f:
    r = requests.post(f'{BASE_URL}/files', files={'file': f})
    print(r.json())

  os.remove(music_path)

