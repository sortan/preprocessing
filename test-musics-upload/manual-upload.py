from audio_helpers import (
  load_wav,
  stereo_to_mono,
  low_pass_filter,
  downsample,
  get_wav_info
)
import requests
import os
import io
from pydub.utils import mediainfo
import json

VALID_SAMPLE_RATE = 44100
VALID_MAX_FREQUENCY = VALID_SAMPLE_RATE / 2
MONO = 1

DOWNLOAD_PATH = os.getcwd() + '/download'
FORMAT = 'wav'
BASE_URL = 'http://localhost:5000'
AUDIO_SAMPLES = [
  'Tena - សរសៃប្រសាទ ( Sor saiprosat ) Psycho​ Remix_Tena Khimphun.wav',
]

for music_path in AUDIO_SAMPLES:
  audio = load_wav(os.path.join(DOWNLOAD_PATH, music_path))
  audio = stereo_to_mono(audio)
  audio = low_pass_filter(audio, VALID_MAX_FREQUENCY)
  audio = downsample(audio, VALID_SAMPLE_RATE)
  audio.export(music_path, format=FORMAT)
  meatadata = mediainfo(music_path)
  # with open('metadata.json', 'w') as f:
  #   json.dump(meatadata, f)


  with open(music_path, 'rb') as f:
    r = requests.post(f'{BASE_URL}/files', files={'file': f})
    print(r.json())

  os.remove(music_path)

