from audio_helpers import (
  load_wav,
  stereo_to_mono,
  low_pass_filter,
  downsample,
  get_wav_info
)
import requests
import os
import io
from pydub.utils import mediainfo
import json
import time

VALID_SAMPLE_RATE = 44100
VALID_MAX_FREQUENCY = VALID_SAMPLE_RATE / 2
MONO = 1

PATH = os.path.join(os.getcwd(), 'segment-clip/20-44100/kom-mean-nek-ti-3/recorded_audio_2021-06-17 21:11:44.505752.wav')
FORMAT = 'wav'
BASE_URL = 'https://api.sortan-core.com'

tic = time.monotonic()
with open(PATH, 'rb') as f:
  r = requests.post(f'{BASE_URL}/musics/search', files={'file': f})
  print(r.json())

tac = time.monotonic()
print(tac - tic)
